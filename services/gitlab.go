package main

import (
	"github.com/gin-gonic/gin"
	"github.com/json-iterator/go"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

const TOKEN = "aQMnEwHsnALXUeKamyTt"
const PROJECT_ID = "11696467"
const COMMITS_URL = "https://gitlab.com/api/v4/projects/:serviceId/repository/commits?since=:since"
const TAGS_URL = "https://gitlab.com/api/v4/projects/:serviceId/repository/tags?order_by=:orderby"
const ONE_TAG_URL = "https://gitlab.com/api/v4/projects/:serviceId/repository/tags/:tagId"

var db = make(map[string]string)

type omit *struct{}

/**
Received struct from Commits Gitlab API
*/
type GitlabCommits struct {
	AuthorEmail    string        `json:"author_email"`
	AuthorName     string        `json:"author_name"`
	AuthoredDate   time.Time     `json:"authored_date"`
	CommittedDate  time.Time     `json:"committed_date"`
	CommitterEmail string        `json:"committer_email"`
	CommitterName  string        `json:"committer_name"`
	CreatedAt      time.Time     `json:"created_at"`
	ID             string        `json:"id"`
	Message        string        `json:"message"`
	ParentIds      []interface{} `json:"parent_ids"`
	ShortID        string        `json:"short_id"`
	Title          string        `json:"title"`
}

/**
Received struct from Tags GitlabAPI
*/
type GitlabTags struct {
	Name    string `json:"name"`
	Message string `json:"message"`
	Target  string `json:"target"`
	Commit  struct {
		ID             string    `json:"id"`
		ShortID        string    `json:"short_id"`
		CreatedAt      time.Time `json:"created_at"`
		ParentIds      []string  `json:"parent_ids"`
		Title          string    `json:"title"`
		Message        string    `json:"message"`
		AuthorName     string    `json:"author_name"`
		AuthorEmail    string    `json:"author_email"`
		AuthoredDate   time.Time `json:"authored_date"`
		CommitterName  string    `json:"committer_name"`
		CommitterEmail string    `json:"committer_email"`
		CommittedDate  time.Time `json:"committed_date"`
	} `json:"commit"`
	Release interface{} `json:"release"`
}

/**
Requested Commits struct
*/
type PublicCommits struct {
	*GitlabCommits
	Target omit `json:"author_name,omitempty"`
}

/**
Requested Tags struct
*/
type PublicTags struct {
	*GitlabTags
	AuthorName omit `json:"target,omitempty"`
}

func setupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	r := gin.Default()

	r.GET("/commits", getCommits)
	r.GET("/tags", getTags)
	r.GET("/tags/:tagId", getOneTag)
	r.POST("/tags", postOneTag)
	return r
}

/**
Transform Gitlab returned
JSON into requested json
*/
func getCommits(c *gin.Context) {
	// "2019-04-08T09:12:14Z"
	since_date := c.Query("since")
	serviceId := c.Query("serviceId")
	var data []GitlabCommits
	// on récupère un tableau de commits depuis Gitlab
	err2 := json.Unmarshal(getCommitsSinceFromGitlab(serviceId, since_date), &data)
	results := make([]PublicCommits, len(data))
	if err2 != nil {
		// handle error
		panic(err2)
	}
	for i := range data {
		var newV []byte
		// On refactor chaque élément du tableau
		// avec la structure de sortie
		newV, _ = json.Marshal(PublicCommits{
			GitlabCommits: &data[i],
		})
		// on réécris les bytes dans le tableau de résultat
		json.Unmarshal(newV, &results[i])
	}

	c.JSON(200, results)
}

/**
Transform Gitlab returned
JSON into requested json
*/
func getTags(c *gin.Context) {
	// "2019-04-08T09:12:14Z"
	serviceId := c.Query("serviceId")
	var data []GitlabTags
	// on récupère un tableau de commits depuis Gitlab
	err2 := json.Unmarshal(getTagsOrderedByName(serviceId), &data)
	results := make([]PublicTags, len(data))
	if err2 != nil {
		// handle error
		panic(err2)
	}
	for i := range data {
		var newV []byte
		// On refactor chaque élément du tableau
		// avec la structure de sortie
		newV, _ = json.Marshal(PublicTags{
			GitlabTags: &data[i],
		})
		// on réécris les bytes dans le tableau de résultat
		json.Unmarshal(newV, &results[i])
	}

	c.JSON(200, results)
}

/**
Transform Gitlab returned
JSON into requested json
*/
func getOneTag(c *gin.Context) {
	// "2019-04-08T09:12:14Z"
	serviceId := c.Query("serviceId")
	tagId := c.Param("tagId")
	var data GitlabTags
	var result PublicTags
	// on récupère le tag voulu depuis Gitlab
	err2 := json.Unmarshal(getSingleTag(serviceId, tagId), &data)
	if err2 != nil {
		// handle error
		panic(err2)
	}
	var newV []byte
	// On refactor chaque élément du tableau
	// avec la structure de sortie
	newV, _ = json.Marshal(PublicTags{
		GitlabTags: &data,
	})
	// on réécris les bytes dans le tableau de résultat
	json.Unmarshal(newV, &result)
	c.JSON(200, result)
}

/**
Transform Gitlab returned
JSON into requested json
*/
func postOneTag(c *gin.Context) {
	// "2019-04-08T09:12:14Z"
	serviceId := c.PostForm("serviceId")
	tagId := c.PostForm("tagId")
	commitRef := c.PostForm("commitRef")
	var data GitlabTags
	var result PublicTags
	// on récupère le tag voulu depuis Gitlab
	err2 := json.Unmarshal(createTag(serviceId, tagId, commitRef), &data)
	if err2 != nil {
		// handle error
		panic(err2)
	}
	var newV []byte
	// On refactor chaque élément du tableau
	// avec la structure de sortie
	newV, _ = json.Marshal(PublicTags{
		GitlabTags: &data,
	})
	// on réécris les bytes dans le tableau de résultat
	json.Unmarshal(newV, &result)
	c.JSON(200, result)
}

/**
GITLAB API
Call to get the all commits
in bytes
*/
func getCommitsSinceFromGitlab(serviceId string, date string) []byte {
	var client = &http.Client{}
	// URL construct
	var URL = strings.Replace(COMMITS_URL, ":serviceId", serviceId, -1)
	URL = strings.Replace(URL, ":since", date, -1)
	// Query construct
	req, err := http.NewRequest("GET", URL, nil)
	if err != nil {
		// handle error
		println("error")
	}
	req.Header.Set("PRIVATE-TOKEN", TOKEN)
	// Query call
	resp, err := client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		// handle error
		println("error")
	}
	body, err := ioutil.ReadAll(resp.Body)

	return body
}

/**
GITLAB API
Call to get the all tags from a gitlab project
in bytes
*/
func getTagsOrderedByName(serviceId string) []byte {
	var client = &http.Client{}
	// URL construct
	var URL = strings.Replace(TAGS_URL, ":serviceId", serviceId, -1)
	URL = strings.Replace(URL, ":orderby", "name", -1)
	// Query construct
	req, err := http.NewRequest("GET", URL, nil)
	if err != nil {
		// handle error
		println("error")
	}
	req.Header.Set("PRIVATE-TOKEN", TOKEN)
	// Query call
	resp, err := client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		// handle error
		println("error")
	}
	body, err := ioutil.ReadAll(resp.Body)

	return body
}

/**
GITLAB API
Call to get one specific tag from
a project (serviceId, tagId)
*/
func getSingleTag(serviceId string, tagId string) []byte {
	var client = &http.Client{}
	// URL construct
	var URL = strings.Replace(ONE_TAG_URL, ":serviceId", serviceId, -1)
	URL = strings.Replace(URL, ":tagId", tagId, -1)
	// Query construct
	req, err := http.NewRequest("GET", URL, nil)
	if err != nil {
		// handle error
		println("error")
	}
	req.Header.Set("PRIVATE-TOKEN", TOKEN)
	// Query call
	resp, err := client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		// handle error
		println("error")
	}
	body, err := ioutil.ReadAll(resp.Body)

	return body
}

/**
GITLAB API
Call to post a new tag, given a
projectId, tagId and commit reference
return the new tag in byte array
*/
func createTag(serviceId string, tagId string, commitRef string) []byte {
	var client = &http.Client{}
	// URL construct
	var URL = strings.Replace(TAGS_URL, ":serviceId", serviceId, -1)
	// Query construct
	data := url.Values{}
	data.Set("tag_name", tagId)
	data.Set("ref", commitRef)
	req, err := http.NewRequest("POST", URL, strings.NewReader(data.Encode()))
	if err != nil {
		// handle error
		println("error")
	}
	req.Header.Set("PRIVATE-TOKEN", TOKEN)
	req.Header.Set("Content-Length", strconv.Itoa(len(data.Encode())))
	// Query call
	resp, err := client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		// handle error
		println("error")
	}
	body, err := ioutil.ReadAll(resp.Body)

	return body
}

func main() {
	r := setupRouter()
	// Listen and Server in 0.0.0.0:8080
	r.Run(":8080")
}
